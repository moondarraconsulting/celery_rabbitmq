from tasks import add
import time
from random import randint

while True:
	print("I'm a worker bee")
	result = add.delay(time.time(), randint(0, 9))
	time.sleep(1)
	print("The result is:")
	print(result.result)
	time.sleep(3)