from celery import Celery

app = Celery('main_tasks',backend='rpc://',broker='pyamqp://guest:guest@rabbit:5672/')

@app.task
def add(x, y):
    return x + y

